# Elasticsearch sample application
Download elasticsearch : https://www.elastic.co/downloads/elasticsearch
Unzip elasticsearch and run bin/elasticsearch


## Run the application on terminal
```mvn clean compile spring-boot:run```

## Run these end points in order
1. Check elastic search information : GET localhost:8080/es-info
2. Create index : POST localhost:8080/user-index?name=users
3. Store document in index : POST localhost:8080/users
 with body
 ```
 {
 	"id" : 1,
 	"username" : "luke_skywalker",
 	"firstName" : "luke",
 	"lastName" : "skywalker",
 	"type": "original"
 }
