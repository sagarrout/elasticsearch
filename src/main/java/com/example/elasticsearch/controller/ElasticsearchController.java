package com.example.elasticsearch.controller;

import org.elasticsearch.action.main.MainResponse;
import org.elasticsearch.client.RestHighLevelClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import static org.springframework.http.MediaType.APPLICATION_JSON_UTF8_VALUE;

@RestController
public class ElasticsearchController {

    private RestHighLevelClient esClient;

    public ElasticsearchController(RestHighLevelClient restHighLevelClient) {
        this.esClient = restHighLevelClient;
    }

    @GetMapping(value = "/es-info", produces = APPLICATION_JSON_UTF8_VALUE)
    public Map<String, String> getClusterName() throws IOException {

        MainResponse response = esClient.info();
        Map<String, String> esInfo = new HashMap<>();


        esInfo.put("ClusterName", response.getClusterName().toString());
        esInfo.put("version", response.getVersion().toString());
        esInfo.put("nodeName", response.getNodeName());
        esInfo.put("build", response.getBuild().toString());

        return esInfo;
    }
}
