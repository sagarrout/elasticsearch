package com.example.elasticsearch.controller;

import com.example.elasticsearch.domain.User;
import com.example.elasticsearch.service.UserService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import static org.springframework.http.MediaType.APPLICATION_JSON_UTF8_VALUE;

@RestController
public class UserController {

    private static final Logger LOGGER = LoggerFactory.getLogger(UserController.class);
    private final UserService userService;

    public UserController(UserService userService) {
        this.userService = userService;
    }

    @PostMapping("/user-index")
    public void createIndex(@RequestParam("name") String indexName) {
        if (indexName != null) {
            userService.createIndex(indexName);
        } else {
            LOGGER.error(indexName);
        }
    }

    @PostMapping(value = "/users", consumes = APPLICATION_JSON_UTF8_VALUE)
    public void post(@RequestBody User user) {
        userService.add(user);
    }
}
