package com.example.elasticsearch.service;

import com.example.elasticsearch.domain.User;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.elasticsearch.action.admin.indices.create.CreateIndexRequest;
import org.elasticsearch.action.index.IndexRequest;
import org.elasticsearch.client.Response;
import org.elasticsearch.client.RestHighLevelClient;
import org.elasticsearch.common.settings.Settings;
import org.elasticsearch.common.xcontent.XContentType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpMethod;
import org.springframework.stereotype.Service;

import java.io.IOException;

@Service
public class UserServiceImpl implements UserService {

    private static final Logger LOGGER = LoggerFactory.getLogger(UserServiceImpl.class);

    private RestHighLevelClient esClient;
    private ObjectMapper objectMapper;

    public UserServiceImpl(RestHighLevelClient esClient, ObjectMapper objectMapper) {
        this.esClient = esClient;
        this.objectMapper = objectMapper;
    }

    @Override
    public void createIndex(String indexName) {

        try {
            Response response = esClient.getLowLevelClient().performRequest(HttpMethod.HEAD.name(), "/" + indexName);
            if (response.getStatusLine().getStatusCode() == 404) {

                CreateIndexRequest createIndexRequest = new CreateIndexRequest(indexName);
                createIndexRequest.settings(Settings.builder()
                        .put("index.number_of_shards", 1)
                        .put("index.number_of_replicas", 1));

                esClient.indices().create(createIndexRequest);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void add(User user) {
        IndexRequest indexRequest = new IndexRequest("users", user.getType(), user.getId().toString());

        LOGGER.info(user.toString());
        try {
            indexRequest.source(objectMapper.writeValueAsString(user), XContentType.JSON);
            esClient.index(indexRequest);
        } catch (IOException e) {
            LOGGER.error(e.getMessage());
        }
    }
}
