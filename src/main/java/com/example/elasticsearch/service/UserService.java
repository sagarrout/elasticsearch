package com.example.elasticsearch.service;

import com.example.elasticsearch.domain.User;

public interface UserService {

    void createIndex(String indexName);

    void add(User user);
}
